#pragma once
#include <string>

using ParsedOutput = std::string;

ParsedOutput removeComments(std::string inputString);
