#include <gtest/gtest.h>

#include <scanner.hpp>


using namespace std;

class NorbertTest : public ::testing::Test
{
};

TEST_F(NorbertTest, emptyString)
{
    string inputString = "";
    string outputString = "";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, onlyLineComment)
{
    string inputString = "//this is comment";
    string outputString = "";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, oneLineComment)
{
    string inputString = "not in comment//comment";
    string outputString = "not in comment";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, blockInOneLineComment)
{
    string inputString = "///*\n*/";
    string outputString = "\n*/";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, oneLineInBLock)
{
    string inputString = "/* test // this should be in block too*/";
    string outputString = "";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, commentsInString) {
    string inputString = "\"//test\" visible";
    string outputString = inputString;
    EXPECT_EQ(outputString, removeComments(inputString));

    outputString = inputString = "\"test/*visible\"*/";
    EXPECT_EQ(outputString, removeComments(inputString));

    outputString = inputString = "\"normal string\"";
    EXPECT_EQ(outputString, removeComments(inputString));
}


TEST_F(NorbertTest, multilineComments)
{
    string inputString = "/*\n\n\n*/";
    string outputString = "";
    EXPECT_EQ(outputString, removeComments(inputString));

    inputString = "//\\\n\\\n";
    outputString = "";
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, javaDoc) {
    string inputString = "/**javadoc\n*/";
    string outputString = inputString;
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, noCommentsMultiLines) {
    string inputString = "first line\nsecond line\n";
    string outputString = inputString;
    EXPECT_EQ(outputString, removeComments(inputString));
}

TEST_F(NorbertTest, gebalaTest)
{
	string inputString = R"(#include<iostream>

/** Maly przyklad programu
 *
 *  autor Maciej Gebala
 */

// /*
using namespace std;
// */

int main()
{
  /// Komentarz dokumentacyjny \
  ciag dalszy jednolinijkowego komentarzaf
  cout << "Poczatek komentarza /*" << endl; // ala
  cout << "Koniec komentarza */ "<< endl; // kot
  cout << "Komentarz /* ala */" << endl;
  cout << "Komentarz // kot " << endl;
  cout << "Zabawa \" // ala i kot " << endl;
  cout << "Pulapka \" \
           // ma \
           /* ma */ \
           " << endl;
 cout << /*Proba*/"Zabawa \" // ala i kot " << endl;\t
 printf("Zabawa \" // ala i kot ");
}
)";
	string outputString = R"(#include<iostream>

/** Maly przyklad programu
 *
 *  autor Maciej Gebala
 */


using namespace std;


int main()
{
  
  cout << "Poczatek komentarza /*" << endl; 
  cout << "Koniec komentarza */ "<< endl; 
  cout << "Komentarz /* ala */" << endl;
  cout << "Komentarz // kot " << endl;
  cout << "Zabawa \" // ala i kot " << endl;
  cout << "Pulapka \" \
           // ma \
           /* ma */ \
           " << endl;
 cout << "Zabawa \" // ala i kot " << endl;\t
 printf("Zabawa \" // ala i kot ");
}
)";
	EXPECT_EQ(outputString, removeComments(inputString));
}