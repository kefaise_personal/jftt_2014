%option noyywrap reentrant stack
%{
#include <scanner.hpp>
#include <iostream>

    ParsedOutput parsedOutput = "";

%}

whitespace [ \t\r]
letter [^ \t\n\r]
newline [\n\0]
%x inString comment oneLineComment javaDoc
%%

<INITIAL>"\"" {BEGIN(inString); parsedOutput += "\"";}
<inString>"\\\"" {parsedOutput += "\\\"";}
<inString>"\\\n" {parsedOutput += "\\\n"; }
<inString>"\"" {BEGIN(INITIAL); parsedOutput += "\"";}
<inString>. {parsedOutput += yytext;}

<INITIAL>"/**" {BEGIN(javaDoc); parsedOutput += yytext;}
<javaDoc>(.|\n) { parsedOutput += yytext;};
<javaDoc>"*/" {BEGIN(INITIAL); parsedOutput += yytext;}

<INITIAL>"//" {BEGIN(oneLineComment);}
<oneLineComment>. {}
<oneLineComment>"\\\n" {}
<oneLineComment>{newline} {BEGIN(INITIAL); parsedOutput += yytext;}

<INITIAL>"/*" {BEGIN(comment);}
<comment>(.|\n) {}
<comment>"*/" BEGIN(INITIAL);

<INITIAL>(.|\n) {parsedOutput += yytext;}

%%

struct YyBuffer
{
    yyscan_t scanner;
    YY_BUFFER_STATE buf;

    YyBuffer(std::string const & inputString)
    {
        yylex_init(&scanner);
        buf = yy_scan_bytes(inputString.c_str(), inputString.size() + 1, scanner);
    }

    ~YyBuffer()
    {
        yy_delete_buffer(buf, scanner);
        yylex_destroy(scanner);
    }
};

ParsedOutput removeComments(std::string inputString)
{
    parsedOutput = "";

    YyBuffer buf(inputString);
	
    yylex(buf.scanner);

    return parsedOutput;
}


