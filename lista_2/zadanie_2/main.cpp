#include <scanner.hpp>
#include <iostream>
#include <sstream>

int main()
{
    std::stringstream str;
    std::string line;
    while(std::getline(std::cin, line))
    {
        str << line;
        str << "\n";
    }
    std::cout << removeComments(str.str());
}
