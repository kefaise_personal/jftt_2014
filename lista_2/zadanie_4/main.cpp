#include <scanner.hpp>
#include <iostream>
#include <sstream>

int main()
{
    std::string line;
    while(std::getline(std::cin, line))
    {
        try
        {
            const int computation = calculate(line);
            std::cout << "= " << computation << std::endl;
        }
        catch(ComputeException cex)
        {
            std::cerr << "error: " << cex.getMessage() << std::endl;
        }
    }
}
