#pragma once
#include <string>
#include <exception>

class ComputeException : public std::exception
{
public:
    ComputeException(std::string message)
    {
        m_message = message;
    }
    std::string getMessage() const
    {
        return m_message;
    }

private:
    std::string m_message;
};

using ParsedOutput = std::string;

int calculate(std::string inputString);
