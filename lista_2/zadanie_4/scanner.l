%option noyywrap reentrant stack
%{
#include <scanner.hpp>
#include <math.h>
#include <vector>
#include <string>
using namespace std;

bool error = false;
bool wasInput = false;
string errorMessage = "";
vector <int> calc;
int computation;
%}

DIGIT [0-9]

%%

"-"{0,1}{DIGIT}+ { if(!error) calc.push_back(atoi(yytext)); wasInput = true; }
"+"|"-"|"*"|"/"|"^"|"%" {
    if(!error) if(calc.size() >= 2) {
        wasInput = true;
        char op = yytext[0];
        int b = calc.back();
        calc.pop_back();
        int a = calc.back();
        calc.pop_back();
        int c = 0;
        if(op == '+') {
            c = a + b;
        } else if(op == '-') {
            c = a - b;
        } else if(op == '*') {
            c = a * b;
        } else if(op == '/') {
			if (b == 0)
			{
				error = true;
				errorMessage = "Pamietaj cholero...";
			}
			else
			{
				c = a / b;
			}
        } else if(op == '^') {
            c = (int)pow((double)a, (double)b);
        } else if(op == '%') {
			if (b == 0)
			{
				error = true;
				errorMessage = "Pamietaj cholero...";
			}
			else
			{
				c = a % b;
			}
        }
        //printf("op: %d%s%d=%d\n", a, yytext, b, c);
        calc.push_back(c);
    } else {
        error = true;
        errorMessage = "Za malo argumentow";
    }
}
[\n\0] {
    if(!error) {
        if(!wasInput)
        {
            computation = 0;
        }
        else if(calc.size() == 1) {
            computation = calc.back();
        } else {
            throw ComputeException("Za malo operatorow");
    }
    } else {
        throw ComputeException(errorMessage.c_str());
    }
    error = false;
    errorMessage = "";
    calc.clear();
}
[ \t\r] ;
. {
    error = true;
    errorMessage = "Nieznane znaki: \"";
    errorMessage += yytext;
    errorMessage += "\"";
}
%%

struct YyBuffer
{
    yyscan_t scanner;
    YY_BUFFER_STATE buf;

    YyBuffer(std::string const & inputString)
    {
        yylex_init(&scanner);
        buf = yy_scan_bytes(inputString.c_str(), inputString.size() + 1, scanner);
    }

    ~YyBuffer()
    {
        yy_delete_buffer(buf, scanner);
        yylex_destroy(scanner);
    }
};

int calculate(std::string inputString)
{
    wasInput = false;
    error = false;
    errorMessage = "";
    computation = 0;
    calc.clear();

    YyBuffer buf(inputString);

    yylex(buf.scanner);


    return computation;
}

