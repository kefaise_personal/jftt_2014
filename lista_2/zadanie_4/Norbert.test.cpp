#include <gtest/gtest.h>

#include <scanner.hpp>


using namespace std;

class NorbertTest : public ::testing::Test
{
};

TEST_F(NorbertTest, noInput)
{
    string inputString = "";
    int output = 0;
    EXPECT_EQ(output, calculate(inputString));
}

TEST_F(NorbertTest, simple)
{
    string inputString = "2 2 +";
    int output = 4;
    EXPECT_EQ(output, calculate(inputString));
}

TEST_F(NorbertTest, invalidInput) {
    string inputString = "2.4 3+";
    ASSERT_THROW(calculate(inputString), ComputeException);

    inputString = "+";
    ASSERT_THROW(calculate(inputString), ComputeException);

    inputString = "1 1";
    ASSERT_THROW(calculate(inputString), ComputeException);

    inputString = "1 +";
    ASSERT_THROW(calculate(inputString), ComputeException);
}

TEST_F(NorbertTest, longCalculation)
{
    string inputString = "8 -7 6 -5 4 * -3 % / - +";
    const int output = 4;
    EXPECT_EQ(output, calculate(inputString));
}

TEST_F(NorbertTest, powTest)
{
    string inputString = "2 3 2^^";
    const int output = 512;
    EXPECT_EQ(output, calculate(inputString));
}

TEST_F(NorbertTest, GotfrydTest)
{
	string inputString = "3 2 2 - /";
	const int output = 0;
	ASSERT_THROW(calculate(inputString), ComputeException);
}