#include <gtest/gtest.h>

#include <scanner.hpp>


using namespace std;

class NorbertTest : public ::testing::Test
{
};

TEST_F(NorbertTest, emptyString)
{
    ParsedOutput ret = removeBlankLinesAndCountLines("");
    EXPECT_EQ(ret.numberOfLines, 0);
    EXPECT_EQ(ret.numberOfWords, 0);
    EXPECT_EQ(ret.parsedInput, "");
}

TEST_F(NorbertTest, simpleTest)
{
    string inputString = " aba \t baba ";
    ParsedOutput ret = removeBlankLinesAndCountLines(inputString);
    EXPECT_EQ(ret.numberOfLines, 1);
    EXPECT_EQ(ret.numberOfWords, 2);
    string outputString = "aba baba";
    EXPECT_EQ(ret.parsedInput, outputString);
}

TEST_F(NorbertTest, complexTest)
{
    string inputString =
		    "\n\n"
            "ignore below line\n"
            "\t   \t    \n"
            " trim\n"
            "trim \n"
            " trim \n"
            "\ttrim\n"
            "trim\t\n"
            "\ttrim\t\n"
            " \t trim \t \n"
            " \t trim\t ";
    ParsedOutput ret = removeBlankLinesAndCountLines(inputString);
    EXPECT_EQ(ret.numberOfLines, 9);
    EXPECT_EQ(ret.numberOfWords, 11);
    string outputString =
            "ignore below line\n"
            "trim\n"
            "trim\n"
            "trim\n"
            "trim\n"
            "trim\n"
            "trim\n"
            "trim\n"
            "trim";
    EXPECT_EQ(ret.parsedInput, outputString);
}

