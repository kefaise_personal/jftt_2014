#include <scanner.hpp>
#include <iostream>
#include <sstream>

int main()
{
    std::stringstream str;
    std::string line;
    while(std::getline(std::cin, line))
    {
        str << line;
        str << "\n";
    }
    ParsedOutput out = removeBlankLinesAndCountLines(str.str());

    std::cout << "=================================" << std::endl;
    std::cout << "number of lines: " << out.numberOfLines << std::endl;
    std::cout << "number of words: " << out.numberOfWords << std::endl;
    std::cout << "parsed output: " << std::endl << out.parsedInput;

}
