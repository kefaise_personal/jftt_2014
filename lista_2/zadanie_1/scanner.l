%option noyywrap reentrant
%{
#include <scanner.hpp>

bool wasWord = false;
ParsedOutput output{0, 0, ""};
%}

whitespace [ \t\r]
letter [^ \t\n\r\0]
newline [\n\0]

%%

^{whitespace}+ ;
{letter}+ {
    output.parsedInput += yytext;
    output.numberOfWords += 1;
    wasWord = true;
}

{whitespace}+ {
    output.parsedInput += " ";
}

{whitespace}+/{newline} {}

{newline} {
    if(wasWord) {
        output.numberOfLines += 1;
        output.parsedInput += yytext;
    }
    wasWord = false;
}

%%

struct YyBuffer
{
    yyscan_t scanner;
    YY_BUFFER_STATE buf;

    YyBuffer(std::string const & inputString)
    {
        yylex_init(&scanner);
        buf = yy_scan_bytes(inputString.c_str(), inputString.size() + 1, scanner);
    }

    ~YyBuffer()
    {
        yy_delete_buffer(buf, scanner);
        yylex_destroy(scanner);
    }
};

ParsedOutput removeBlankLinesAndCountLines(std::string inputString)
{
    output = {0, 0, ""};
    wasWord = false;

    YyBuffer buf(inputString);
	
    yylex(buf.scanner);

    return output;
}


