#pragma once
#include <string>

using NumberOfLines = unsigned int;
using NumberOfWords = unsigned int;
using ParsedInput = std::string;

struct ParsedOutput
{
    NumberOfLines numberOfLines;
    NumberOfWords numberOfWords;
    ParsedInput parsedInput;
};

ParsedOutput removeBlankLinesAndCountLines(std::string inputString);
