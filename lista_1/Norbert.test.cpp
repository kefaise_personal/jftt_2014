#include <string>
#include <gtest/gtest.h>

#include <Pattern.h>


using namespace std;

class NorbertTest : public ::testing::Test
{
};

TEST_F(NorbertTest, emptyString)
{
	string s = "";
	string pattern = "pattern";
	EXPECT_EQ(-1, searchPattern(s, pattern));

	pattern = "";
	EXPECT_EQ(0, searchPattern(s, pattern));
}

TEST_F(NorbertTest, simpleTest)
{
    string s = "a";
    string pattern = "a";
    EXPECT_EQ(0, searchPattern(s, pattern));

    s = "ab";
    pattern = "b";
    EXPECT_EQ(1, searchPattern(s, pattern));
}

TEST_F(NorbertTest, normalCases)
{
    string input = "Donec at metus in nibh pulvinar sodales at vel leo. Suspendisse a enim dui. "
            "Nulla sed laoreet nulla. Maecenas fringilla quis diam et euismod. Sed eu nunc eu enim tempor tristique non eget ante. "
            "Donec eu dignissim lorem. Mauris lacinia tellus eget sem convallis mattis. Aenean ac iaculis erat. "
            "Morbi rhoncus blandit turpis ac lacinia. Curabitur sit amet dolor molestie, dignissim nulla eu, finibus velit. "
            "Pellentesque porttitor dui vitae justo accumsan, vel viverra velit iaculis. "
            "Fusce viverra metus mi, eu lobortis nibh bibendum mattis. In pellentesque volutpat metus, ac commodo justo rutrum quis. "
            "Donec lacinia, nisi quis maximus consequat, dolor erat consectetur tellus, sit amet porttitor mi sapien sed velit. "
            "Proin et magna sed tellus euismod fermentum. Mauris pellentesque sapien velit, sit amet viverra est aliquam vitae. ";
    string pattern = "in nibh pulvinar sodales at vel leo";
    EXPECT_EQ(15, searchPattern(input, pattern));

	pattern = "Morbi";
	EXPECT_EQ(293, searchPattern(input, pattern));

}

TEST_F(NorbertTest, emptyPattern)
{
    string input = "sdfjsd";
    string pattern = "";
    EXPECT_EQ(input.size(), searchPattern(input, pattern));
}

TEST_F(NorbertTest, patternNotFound)
{
    string input = "abc";
    string pattern = "abcd";
    EXPECT_EQ(-1, searchPattern(input, pattern));
}

TEST_F(NorbertTest, upperCaseLetters)
{
	string input = "AbC";
	string pattern = "Bc";
	EXPECT_EQ(1, searchPattern(input, pattern));
}