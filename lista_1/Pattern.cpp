#include <Pattern.h>
#include <algorithm>

using namespace std;

bool startsWith(string const & ActualString, string const & ComparisonString) {
    return ComparisonString.size() == 0 ||
            (ComparisonString.length() <= ActualString.length() &&
             equal(ComparisonString.begin(), ComparisonString.end(), ActualString.begin()));
}

const string alphabet = "abcdefghijklmnopqrstuvwxyz., ";

StateMachine createPatternStateMachine(string const & pattern)
{
    StateMachine machine;
    Position m = pattern.size();
    for(Position q = 0; q <= m; ++q)
    {
        for(char a : alphabet)
        {
            State k = std::min(m + 1, q + 2);
            do
            {
                k -= 1;
            }
            while(k > 0 && !startsWith(pattern.substr(0, k), pattern.substr(0, q) + a) );

            const StateTransition transition(q, a);
            machine.insert(make_pair(transition, k));
        }
    }
    return machine;
}

Position matchPattern(string const & inputString, StateMachine const & machine, const Position m)
{
    State q = 0;
    for(Position i = 0; i < inputString.size(); ++i)
    {
        q = machine.at(make_pair(q, inputString[i]) );
        if(q == m)
        {
            return 1 + i - m;
        }
    }
    return -1;
}

Position searchPattern(string inputString, string pattern)
{
    if(pattern.size() == 0)
    {
        return inputString.size();
    }

	std::transform(inputString.begin(), inputString.end(),
		inputString.begin(),
                   ::tolower);
	std::transform(pattern.begin(), pattern.end(),
		pattern.begin(),
		::tolower);

	StateMachine machine = createPatternStateMachine(pattern);
	return matchPattern(inputString, machine, pattern.size());
}
