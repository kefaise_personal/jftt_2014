#include <string>
#include <iostream>
#include <Pattern.h>

void patternNotFoundMessage(std::string const & inputString, std::string const & pattern)
{
    std::cout << "pattern: '" << pattern << "' not found in: '" << inputString << "'." << std::endl;
}

void patternFoundMessage(std::string const & inputString, std::string const & pattern, const int position)
{
    std::cout << "pattern: '" << pattern << "' found in: '" << inputString << "' on position: " << position << "." << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        std::cerr << "Not enough arguments" << std::endl;
    }
    if((argc - 1) % 2 != 0)
    {
        --argc;
    }
    for(int i = 2; i < argc; i+=2)
    {
        const std::string inputString = argv[i-1];
        const std::string pattern = argv[i];
        const int position = searchPattern(inputString, pattern);
        position >= 0 ? patternFoundMessage(inputString, pattern, position) : patternNotFoundMessage(inputString, pattern);
    }
}
