#include <string>
#include <utility>
#include <map>
#include <vector>

using State = std::string::size_type;
using StateTransition = std::pair < State, char >;
using StateMachine = std::map<StateTransition, State>;
using Position = std::string::size_type;

Position searchPattern(std::string inputString, std::string pattern);
