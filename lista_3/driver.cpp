#include <cctype>
#include <fstream>
#include <cassert>
#include <sstream>

#include "driver.hpp"

Calculator::Driver::~Driver(){ 
   delete(scanner);
   scanner = nullptr;
   delete(parser);
   parser = nullptr;
}

void Calculator::Driver::parse( std::string input )
{
   std::istringstream inputStream(input);
   m_errorStream.str("");
   
   m_result = 0.0;
   
   delete(scanner);
   try
   {
      scanner = new Calculator::Scanner( &inputStream );
   }
   catch( std::bad_alloc &ba )
   {
        m_errorStream << "Failed to allocate scanner: (" <<
            ba.what() << ")\n";
	    return;
   }
   
   delete(parser); 
   try
   {
      parser = new Calculator::Parser( (*scanner) /* scanner */, 
                                  (*this) /* driver */ );
   }
   catch( std::bad_alloc &ba )
   {
        m_errorStream << "Failed to allocate parser: (" << 
            ba.what() << "\n";
		return;
   }
   
   parser->parse();
}

double Calculator::Driver::getResult() const
{
    return m_result;
}

void Calculator::Driver::setResult(double result)
{
    m_result = result;
}

void Calculator::Driver::addError(std::string const & error)
{
    m_errorStream << error;
}

std::string Calculator::Driver::getError() const
{
    return m_errorStream.str();
}

bool Calculator::Driver::isError() const
{
    return m_errorStream != "";
}

std::ostream& Calculator::Driver::print(std::ostream &stream)
{
	stream << m_result;
	return stream;
}
