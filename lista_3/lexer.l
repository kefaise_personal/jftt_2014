%{
/* C++ string header, for string ops below */
#include <string>
/* Implementation of yyFlexScanner */ 
#include "scanner.hpp"

/* typedef to make the returns for the tokens shorter */
using token=Calculator::Parser::token;

/* define to keep from re-typing the same code over and over */
#define STOKEN( x ) ( new std::string( x ) )

/* define yyterminate as this instead of NULL */
#define yyterminate() return( token::END )

/* msvc2010 requires that we exclude this header file. */
#define YY_NO_UNISTD_H

%}

%option debug 
%option nodefault 
%option yyclass="Calculator::Scanner" 
%option noyywrap 
%option c++

%x oneLineComment Error

%%
<INITIAL>[#] BEGIN(oneLineComment);
<oneLineComment>.
<oneLineComment>[\n] BEGIN(INITIAL);

<INITIAL>
[0-9]+ {
            yylval->number = atof(yytext);
            return token::NUMBER;
        }
<INITIAL>
[0-9]+[.][0-9]+ {
                    yylval->number = atof(yytext); 
		            return token::NUMBER;
                
				}
			
<INITIAL>
[\+] {
	    yylval->symbol = yytext[0]; 
		return token::OP_ADD;
    }
<INITIAL>
[\-] {
	    yylval->symbol = yytext[0]; 
		return token::OP_MINUS;
    }
	
<INITIAL>
[ \t]+ { }

<<EOF>>  { return token::END; }

<INITIAL>[\*]   { yylval->symbol = yytext[0]; return token::OP_MULTIPLY; }
<INITIAL>[/]   { yylval->symbol = yytext[0]; return token::OP_DIVIDE; }
<INITIAL>[\^]   { yylval->symbol = yytext[0]; return token::OP_POWER; }
<INITIAL>"("      { return token::LP; }
<INITIAL>")"      { return token::RP; }


<INITIAL>.+[\n] { 
                    yylval->sval = STOKEN( yytext );
					return token::WRONG_TOKEN;
				}
%%


