%skeleton "lalr1.cc"
%require  "3.0"
%debug 
%defines 
%define api.namespace {Calculator}
%define parser_class_name {Parser}

%code requires{
   namespace Calculator {
      class Driver;
      class Scanner;
   }
}

%parse-param { Scanner  &scanner  }
%parse-param { Driver  &driver  }

%code{
   #include <iostream>
   #include <cstdlib>
   #include <fstream>
   #include <string>
   #include <cmath>
   
   /* include for all driver functions */
   #include "driver.hpp"

#undef yylex
#define yylex scanner.yylex
}

/* token types */
%union {
   std::string *sval;
   double number;
   char symbol;
}

%token   <number> END    0     "end of file"

%token   <number> NUMBER
%token   <symbol> OP_ADD OP_MINUS LP RP
%token   <symbol> OP_MULTIPLY OP_DIVIDE
%token   <symbol> OP_POWER

%left OP_ADD OP_MINUS
%left OP_MULTIPLY OP_DIVIDE
%right OP_POWER
%left NEG

%token <sval> WRONG_TOKEN
%token            NEWLINE

%type <number> exp res 
%type <sval> err

/* destructor rule for <sval> objects */
%destructor { if ($$)  { delete ($$); ($$) = nullptr; } } <sval>


%%
run: res
	| END {driver.setResult(0.0); }
		
res: exp        {driver.setResult($1);}
	| exp err | err
	
exp: NUMBER {$$ = $1;}
	| exp OP_ADD exp {$$ = ($1 + $3);}
	| exp OP_MINUS exp {$$ = ($1 - $3);}
	| exp OP_MULTIPLY exp {$$ = ($1 * $3);}
	| exp OP_DIVIDE exp {$$ = ($1 / $3);}
	| exp OP_POWER exp {$$ = (pow($1, $3));}
	| LP exp RP {$$ = $2;}
	| OP_MINUS exp %prec NEG {$$ = -$2;}

err: WRONG_TOKEN {}
%%


void Calculator::Parser::error( const std::string &err_message )
{
    driver.addError(err_message);
}
