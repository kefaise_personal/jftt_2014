#pragma once

#include <string>
#include <sstream>
#include "scanner.hpp"
#include "parser.tab.hh"

namespace Calculator
{

class Driver{
public:
   Driver() : m_result(0.0),
              parser( nullptr ),
              scanner( nullptr ){};

   virtual ~Driver();

   void parse( std::string input );
  
   double getResult() const;
   void setResult(double result);
   
   void addError(std::string const & error);
   std::string getError() const;
   bool isError() const;

   std::ostream& print(std::ostream &stream);
private:
   double m_result;
   std::stringstream m_errorStream;
   
   Calculator::Parser *parser;
   Calculator::Scanner *scanner;
};

} /* end namespace Calculator */

