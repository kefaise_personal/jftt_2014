#include <iostream>
#include <cstdlib>
#include <string>

#include "driver.hpp"

int 
main(const int argc, const char **argv)
{
	std::string str;
	Calculator::Driver driver;
	
	do
	{
	    getline(std::cin, str);
		if(str == "")
		{
		    break;
		}
		driver.parse(str);
		driver.print(std::cout) << "\n";
	} while(str != "");

   return( EXIT_SUCCESS );
}
